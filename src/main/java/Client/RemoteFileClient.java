package Client;

import Core.ServiceDetails;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class RemoteFileClient {
    public static void main(String[] args) {
        try{
            Socket client = new Socket("127.0.0.1", ServiceDetails.SERVERPORT);
            OutputStream outputToSocket = client.getOutputStream();
            InputStream inputFromSocket = client.getInputStream();

            PrintWriter streamWriter = new PrintWriter(outputToSocket);
            BufferedReader streamReader = new BufferedReader((new InputStreamReader(inputFromSocket)));

            //Send the name of the file
            boolean running = true;
            while(running) {
                String filename = getNameOfFileFromUser();
                streamWriter.println(filename);
                streamWriter.flush();

                String line = null;
                System.out.println("**** File Content ****");
                while (!(line = streamReader.readLine()).equals("EOF")) {
                    System.out.println(line);
                }
                System.out.println("**** File ended ****");
            }
            streamReader.close();
            streamWriter.close();
            client.close();

        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private static String getNameOfFileFromUser(){
        String filename;
        Scanner sc = new Scanner(System.in);
        System.out.println("What file would you like: ");
        filename = sc.nextLine();
        return filename;
    }
}
