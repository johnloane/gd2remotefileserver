package ThreadPoolServer;

import Core.ServiceDetails;
import com.sun.corba.se.spi.activation.Server;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;

public class PooledRemoteFileServer {
    private ServerSocket serverSocket;
    private int listenPort;
    private int maxConnections;

    public PooledRemoteFileServer(int listenPort, int maxConnections) {
        this.listenPort = listenPort;
        this.maxConnections = maxConnections;
    }

    public static void main(String[] args) {
        //Set up the server
        PooledRemoteFileServer server = new PooledRemoteFileServer(ServiceDetails.SERVERPORT, ServiceDetails.MAXCONNECTIONS);

        //Set up the handler threads in a pool
        server.setUpHandlers();
        //Start accepting connections
        server.acceptConnections();
    }

    //Set up the connection pool - create all the threads that will be reused during the lifetime of the server
    public void setUpHandlers(){
        for(int i=0; i < ServiceDetails.MAXCONNECTIONS; ++i){
            PooledConnectionHandler currentHandler = new PooledConnectionHandler();
            Thread t = new Thread(currentHandler);
            t.start();
        }
    }

    public void acceptConnections(){
        try{
            //Open a listening socket
            ServerSocket server = new ServerSocket(ServiceDetails.SERVERPORT, 5);
            Socket incomingConnection = null;

            while(true){
                //Accept the next client
                incomingConnection = server.accept();
                //Handle the client, either with one of our service threads or else out in a queue
                handleConnection(incomingConnection);
            }
        } catch(IOException io){
            io.printStackTrace();
        }
    }

    public void handleConnection(Socket connectionToHandle){
        //Process the client's request to connect to the server
        PooledConnectionHandler.processRequest(connectionToHandle);
    }


}
