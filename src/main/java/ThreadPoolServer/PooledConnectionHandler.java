package ThreadPoolServer;

import java.io.*;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class PooledConnectionHandler implements Runnable {
    private Socket clientSocket;

    //Create a list to store the clients that haven't been processed yet
    private static List pool = new LinkedList();

    public PooledConnectionHandler() {
    }

    public static void processRequest(Socket incomingClient){
        synchronized(pool){
            pool.add(pool.size(), incomingClient);
            //Notify all waiting threads that there is a new client to be serviced
            pool.notifyAll();
        }
    }

    public void run(){
        while(true){
            //Lock the pool, if another thread has the lock then we will have to wait
            synchronized (pool) {
                //While the pool is empty, wait
                while (pool.isEmpty()) {
                    try {
                        //Wait until another client is added to the pool
                        System.out.println("Waiting for connections");
                        pool.wait();
                    } catch (InterruptedException ie) {
                        return;
                    }
                }
                //At this point we have the lock on the pool and it is not empty
                System.out.println("Setting up a socket for the client");
                clientSocket = (Socket) pool.remove(0);
            }
            //Interact with the client
            handleConnection();

        }
    }

    public void handleConnection(){
        try {
            OutputStream outputToSocket = clientSocket.getOutputStream();
            InputStream inputFromSocket = clientSocket.getInputStream();
            PrintWriter streamWriter = new PrintWriter(outputToSocket);
            BufferedReader streamReader = new BufferedReader((new InputStreamReader(inputFromSocket)));
            String fileToRead = null;
            while((fileToRead = streamReader.readLine()) != null) {

                System.out.println("User wants to read " + fileToRead);
                BufferedReader fileReader = new BufferedReader(new FileReader(fileToRead));

                String line = null;
                while ((line = fileReader.readLine()) != null) {
                    System.out.println("Sending " + line);
                    streamWriter.println(line);
                    streamWriter.flush();
                }
                streamWriter.println("EOF");
                streamWriter.flush();
                //fileReader.close();
                Thread.currentThread().sleep(50);
            }

            streamWriter.close();
            streamReader.close();

        } catch (FileNotFoundException e) {
            System.out.println("Could not find the requested file on the server");
        }
        catch(InterruptedException iex){
            iex.printStackTrace();
        }
        catch(IOException io){
            io.printStackTrace();
        }
    }
}
