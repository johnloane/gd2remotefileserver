package ThreadPerClientServer;

import Core.ServiceDetails;

import java.net.ServerSocket;
import java.net.Socket;

public class RemoteFileServer {
    public static void main(String[] args) {
        try{
            //Set up the listening socket
            ServerSocket server = new ServerSocket(ServiceDetails.SERVERPORT);

            while(true){
                //Accept a new client
                Socket clientSocket = server.accept();

                //Create a new handler thread to deal with this client
                ConnectionHandler clientHandler = new ConnectionHandler(clientSocket);
                //Create a wrapper thread
                Thread clientThread = new Thread(clientHandler);
                clientThread.start();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
